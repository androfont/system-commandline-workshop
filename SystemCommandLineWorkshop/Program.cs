﻿using Microsoft.Extensions.Logging;
using Spectre.Console;
using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Help;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.Diagnostics;

namespace SystemCommandLineWorkshop;

internal class Program
{
    static async Task<int> Main(string[] args)
    {
        var returnCode = 0;

        var fileOption = new Option<FileInfo?>(
            name: "--file",
            description: "An option whose argument is parsed as a FileInfo",
            isDefault: true,
            parseArgument: result =>
            {
                if (result.Tokens.Count == 0)
                {
                    return new FileInfo("sampleQuotes.txt");

                }
                string? filePath = result.Tokens.Single().Value;
                if (!File.Exists(filePath))
                {
                    result.ErrorMessage = "File does not exist";
                    return null;
                }
                else
                {
                    return new FileInfo(filePath);
                }
            });
        fileOption.ArgumentHelpName = "FILEPATH";

        var delayOption = new Option<int>(
            name: "--delay",
            description: "Delay between lines, specified as milliseconds per character in a line.",
            getDefaultValue: () => 42);
        delayOption.AddValidator(result =>
        {
            if (result.GetValueForOption(delayOption) < 0)
            {
                result.ErrorMessage = "Delay value must be greather or equal 0.";
            }
        });

        var fgcolorOption = new Option<ConsoleColor>(
            name: "--fgcolor",
            description: "Foreground color of text displayed on the console.",
            getDefaultValue: () => ConsoleColor.White);

        var lightModeOption = new Option<bool>(
            name: "--light-mode",
            description: "Background color of text displayed on the console: default is black, light mode is white.");

        var searchTermsOption = new Option<string[]>(
            name: "--search-terms",
            description: "Strings to search for when deleting entries.")
        {
            IsRequired = true,
            AllowMultipleArgumentsPerToken = true
        };

        var quoteArgument = new Argument<string>(
            name: "quote",
            description: "Text of quote.");

        var bylineArgument = new Argument<string>(
            name: "byline",
            description: "Byline of quote.");

        var rootCommand = new RootCommand("Sample app for System.CommandLine");
        rootCommand.AddGlobalOption(fileOption);

        var quotesCommand = new Command("quotes", "Work with a file that contains quotes.");
        rootCommand.AddCommand(quotesCommand);

        var readCommand = new Command("read", "Read and display the file.")
        {
            delayOption,
            fgcolorOption,
            lightModeOption
        };
        quotesCommand.AddCommand(readCommand);

        var deleteCommand = new Command("delete", "Delete lines from the file.");
        deleteCommand.AddOption(searchTermsOption);
        quotesCommand.AddCommand(deleteCommand);

        var addCommand = new Command("add", "Add an entry to the file.");
        addCommand.AddArgument(quoteArgument);
        addCommand.AddArgument(bylineArgument);
        addCommand.AddAlias("insert");
        quotesCommand.AddCommand(addCommand);

        readCommand.SetHandler(async (context) =>
        {
            var file = context.ParseResult.GetValueForOption(fileOption);
            var delay = context.ParseResult.GetValueForOption(delayOption);
            var fgcolor = context.ParseResult.GetValueForOption(fgcolorOption);
            var lightMode = context.ParseResult.GetValueForOption(lightModeOption);

            returnCode = await ReadFile(file!, delay, fgcolor, lightMode, context.GetCancellationToken());
        });

        deleteCommand.SetHandler((file, searchTerms, logger) =>
        {
            DeleteFromFile(file!, searchTerms, logger);
        }, fileOption, searchTermsOption, new LogBinder());

        addCommand.SetHandler((file, quote, logger) =>
        {
            AddToFile(file!, quote, logger);
        }, fileOption, new QuoteBinder(quoteArgument, bylineArgument), new LogBinder());

        var parser = new CommandLineBuilder(rootCommand)
            .AddMiddleware(BenchmarkMiddleware)
            .UseDefaults()
            .UseHelp(ctx =>
            {
                ctx.HelpBuilder.CustomizeSymbol(fgcolorOption,
                    firstColumnText: x => "--color <Black, White, Red, or Yellow>",
                    secondColumnText: x => "Specifies the foreground color. " +
                        "Choose a color that provides enough contrast " +
                        "with the background color. " +
                        "For example, a yellow foreground can't be read " +
                        "against a light mode background.");
                ctx.HelpBuilder.CustomizeLayout(
                    _ =>
                        HelpBuilder.Default
                            .GetLayout()
                            .Skip(1) // Skip the default command description section.
                            .Prepend(
                                _ => AnsiConsole.Write(
                                    new FigletText(rootCommand.Description!))
                    ));
            })
            .Build();

        await parser.InvokeAsync(args);
        if (returnCode != 0)
        {
            Console.WriteLine(returnCode);
        }
        return returnCode;
    }

    private static async Task BenchmarkMiddleware(InvocationContext context, Func<InvocationContext, Task> next)
    {
        if (context.ParseResult.Directives.Contains("bench"))
        {
            var sw = new Stopwatch();
            sw.Start();
            await next(context);
            sw.Stop();
            AnsiConsole.WriteLine();
            AnsiConsole.Write(new Panel($"Elapsed Time: [aqua]{sw.Elapsed}[/]"));
        }
        else
        {
            await next(context);
        }
    }

    internal static async Task<int> ReadFile(FileInfo file, int delay, ConsoleColor fgColor, bool lightMode, CancellationToken cancellationToken)
    {
        Console.BackgroundColor = lightMode ? ConsoleColor.White : ConsoleColor.Black;
        Console.ForegroundColor = fgColor;
        var lines = File.ReadLines(file.FullName).ToList();
        foreach (string line in lines)
        {
            Console.WriteLine(line);
            await Task.Delay(delay * line.Length);
            if (cancellationToken.IsCancellationRequested)
            {
                return 1;
            }
        };

        return 0;
    }

    internal static void DeleteFromFile(FileInfo file, string[] searchTerms, ILogger logger)
    {
        logger.LogInformation($"Deleting from file terms: {string.Join(" ", searchTerms)}.");
        File.WriteAllLines(
            file.FullName, File.ReadLines(file.FullName)
                .Where(line => searchTerms.All(s => !line.Contains(s))).ToList());
        logger.LogInformation("Deletion successfully.");
    }

    internal static void AddToFile(FileInfo file, Quote quote, ILogger logger)
    {
        logger.LogInformation($"Adding to file quote: \n{quote}");
        using StreamWriter? writer = file.AppendText();
        writer.WriteLine($"{Environment.NewLine}{Environment.NewLine}{quote.Phrase}");
        writer.WriteLine($"{Environment.NewLine}-{quote.Author}");
        writer.Flush();
        logger.LogInformation("Quote added successfully.");
    }
}
