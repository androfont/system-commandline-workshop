﻿using System.CommandLine;
using System.CommandLine.Binding;

namespace SystemCommandLineWorkshop;
internal class QuoteBinder : BinderBase<Quote>
{
    private readonly Argument<string> _phraseArgument;
    private readonly Argument<string> _authorArgument;

    public QuoteBinder(Argument<string> phraseArgument, Argument<string> authorArgument)
    {
        _phraseArgument = phraseArgument;
        _authorArgument = authorArgument;
    }

    protected override Quote GetBoundValue(BindingContext bindingContext)
    {
        var phrase = bindingContext.ParseResult.GetValueForArgument(_phraseArgument) ?? throw new ArgumentNullException();
        var author = bindingContext.ParseResult.GetValueForArgument(_authorArgument) ?? throw new ArgumentNullException();
        return new Quote(phrase, author);
    }
}
