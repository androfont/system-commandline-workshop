﻿namespace SystemCommandLineWorkshop;
internal class Quote
{
    public string Phrase { get; }

    public string Author { get; }

    public Quote(string phrase, string author)
    {
        Phrase = phrase;
        Author = author;
    }

    public override string ToString()
    {
        return $"{Phrase} \n\t-{Author}";
    }
}
