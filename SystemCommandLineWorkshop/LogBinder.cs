﻿using Microsoft.Extensions.Logging;
using System.CommandLine.Binding;

namespace SystemCommandLineWorkshop;
internal class LogBinder : BinderBase<ILogger>
{

    protected override ILogger GetBoundValue(BindingContext bindingContext)
    {
        return GetLogger();
    }

    ILogger GetLogger()
    {
        using ILoggerFactory loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
        ILogger logger = loggerFactory.CreateLogger("ApplicationLogger");
        return logger;
    }
}
