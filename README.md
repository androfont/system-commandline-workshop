
# [System.CommandLine](https://learn.microsoft.com/en-us/dotnet/standard/commandline/)

Source Code: [Andro Font Hdez / system-commandline-workshop · GitLab](https://gitlab.com/androfont/system-commandline-workshop)

## Overview

This command-line application is designed as a usage sample of the `System.CommandLine` NuGet package. System.CommandLine is a powerful library for building command-line interfaces (CLIs) in .NET applications.

## Fundamentals

> By default you can use --help(-h, -?) and --version options
### Create an option & command

```C#
// Creates the option
var fileOption = new Option<FileInfo?>( 
	name: "--file", 
	description: "The file to read and display on the console.");

// Creates de command and add the option
var rootCommand = new RootCommand("Sample app for System.CommandLine");
rootCommand.AddOption(fileOption);

// Set command logic
rootCommand.SetHandler((file) => { ... }, fileOption);
```
### Add a subcommand and options

```C#
var delayOption = new Option<int>(
    name: "--delay",
    description: "Delay between lines, specified as milliseconds per character in a line.",
    getDefaultValue: () => 42);

var fgcolorOption = new Option<ConsoleColor>(
    name: "--fgcolor",
    description: "Foreground color of text displayed on the console.",
    getDefaultValue: () => ConsoleColor.White);

var lightModeOption = new Option<bool>(
    name: "--light-mode",
    description: "Background color of text displayed on the console: default is black, light mode is white.");

var readCommand = new Command("read", "Read and display the file.") 
{ 
	fileOption, 
	delayOption, 
	fgcolorOption, 
	lightModeOption 
};
rootCommand.AddCommand(readCommand);
```
### Add custom validation

```C#
var searchTermsOption = new Option<string[]>( name: "--search-terms", description: "Strings to search for when deleting entries.") 
{ 
	IsRequired = true, 
	AllowMultipleArgumentsPerToken = true 
};
```
`IsRequired = true` makes the option value required.
`AllowMultipleArgumentsPerToken = true` ets you omit the `--search-terms` option name when specifying elements in the list after the first one.
### Add global options

```C#
var fileOption = new Option<FileInfo?>(
	name: "--file",
	description: "An option whose argument is parsed as a FileInfo",
	isDefault: true,
	parseArgument: result =>
	{
		if (result.Tokens.Count == 0)
		{
			return new FileInfo("sampleQuotes.txt");

		}
		string? filePath = result.Tokens.Single().Value;
		if (!File.Exists(filePath))
		{
			result.ErrorMessage = "File does not exist";
			return null;
		}
		else
		{
			return new FileInfo(filePath);
		}
	});

rootCommand.AddGlobalOption(fileOption);
```

Global options are applied to the command and recursively to subcommands. Since `--file` is on the root command, it will be available automatically in all subcommands of the app.
## Command Line Syntax

### Commands

A _command_ in command-line input is a token that specifies an action or defines a group of related actions.
`dotnet run`: `run` is a command that specifies an action.
#### Root Command

The _root command_ is the one that specifies the name of the app's executable.
`dotnet` command specifies the _dotnet.exe_ executable.
#### Subcommands

Most command-line apps support _subcommands_, also known as _verbs_.
`dotnet` command has a `run` subcommand that you invoke by entering `dotnet run`.

Subcommands can have their own subcommands.
`dotnet tool install`, `install` is a subcommand of `tool`.
### Options

An option is a named parameter that can be passed to a command. Values of options can be specify implicit or explicit for ex.
`dotnet tool update dotnet-suggest --verbosity quiet --global` verbosity has explicit quiet value, global implicit
### Arguments

An argument is a value passed to an option or a command.
`dotnet tool update dotnet-suggest --verbosity quiet --global` quiet is an argument of verbosity.
Arguments can have default values that apply if no argument is explicitly provided. This is very common with Boolean options.
### Order of options and arguments

You can provide options before arguments or arguments before options on the command line.
Options can be specified in any order.
When there are multiple arguments, the order does matter.
### The `--` token

POSIX convention interprets the double-dash (`--`) token as an escape mechanism. Everything that follows the double-dash token is interpreted as arguments for the command.
The following command line uses the double-dash token to set the value of the `message` argument to "--interactive":
`myapp -- --interactive`
### Option-argument delimiters

`System.CommandLine` lets you use a space, '=', or ':' as the delimiter between an option name and its argument.
### Argument arity

`System.CommandLine` has an [ArgumentArity](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.argumentarity) struct for defining arity, with the following values:
- [Zero](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.argumentarity.zero#system-commandline-argumentarity-zero) - No values allowed.
- [ZeroOrOne](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.argumentarity.zeroorone#system-commandline-argumentarity-zeroorone) - May have one value, may have no values.
- [ExactlyOne](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.argumentarity.exactlyone#system-commandline-argumentarity-exactlyone) - Must have one value.
- [ZeroOrMore](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.argumentarity.zeroormore#system-commandline-argumentarity-zeroormore) - May have one value, multiple values, or no values.
- [OneOrMore](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.argumentarity.oneormore#system-commandline-argumentarity-oneormore) - May have multiple values, must have at least one value.
### Response files

A _response file_ is a file that contains a set of [tokens](https://learn.microsoft.com/en-us/dotnet/standard/commandline/syntax#tokens) for a command-line app. Response files are a feature of `System.CommandLine` that is useful in two scenarios:
- To invoke a command-line app by specifying input that is longer than the character limit of the terminal.
- To invoke the same command repeatedly without retyping the whole line.
To use a response file, enter the file name prefixed by an `@` sign wherever in the line you want to insert commands, options, and arguments. The _.rsp_ file extension is a common convention, but you can use any file extension.
`dotnet @sample1.rsp`
### Directives

The purpose of directives is to provide cross-cutting functionality that can apply across command-line apps. Because directives are syntactically distinct from the app's own syntax, they can provide functionality that applies across apps.

A directive must conform to the following syntax rules:

- It's a token on the command line that comes after the app's name but before any subcommands or options.
- It's enclosed in square brackets.
- It doesn't contain spaces.

The following directives are built in:
- [`[parse]`](https://learn.microsoft.com/en-us/dotnet/standard/commandline/syntax#the-parse-directive)
- [`[suggest]`](https://learn.microsoft.com/en-us/dotnet/standard/commandline/syntax#the-suggest-directive)
### Design guidance
- If a command has subcommands, the command should function as an area, or a grouping identifier for the subcommands, rather than specify an action.
- Avoid using any of the following aliases differently than their common usage in the .NET CLI and other .NET command-line apps:
	- `-i` for `--interactive`.
	    
	    This option signals to the user that they may be prompted for inputs to questions that the command needs answered. For example, prompting for a username. Your CLI may be used in scripts, so use caution in prompting users that have not specified this switch.
	    
	- `-o` for `--output`.
	    
	    Some commands produce files as the result of their execution. This option should be used to help determine where those files should be located. In cases where a single file is created, this option should be a file path. In cases where many files are created, this option should be a directory path.
	    
	- `-v` for `--verbosity`.
- Define names in lowercase only, except you can make uppercase aliases to make commands or options case insensitive.
- Use [kebab case](https://en.wikipedia.org/wiki/Letter_case#Kebab_case) to distinguish words. For example, `--additional-probing-path`.
- Use verbs rather than nouns for commands that refer to actions
- `System.CommandLine` applications typically offer a `--verbosity` option that specifies how much output is sent to the console. Here are the standard five settings:
	- `Q[uiet]`
	- `M[inimal]`
	- `N[ormal]`
	- `D[etailed]`
	- `Diag[nostic]`
## Defining Commands

### Defining Alias

Option alias:
```C#
var option = new Option("--framework");
option.AddAlias("-f");
```

Command alias:
```C#
var command = new Command("serialize");
command.AddAlias("serialise");
```
### Required options

```C#
var endpointOption = new Option<Uri>("--endpoint") { IsRequired = true };
```
###  Hidden commands, options, and arguments

```C#
var endpointOption = new Option<Uri>("--endpoint") { IsHidden = true };
```
### Set argument arity

```C#
var searchTermsOption = new Option<string[]>(
    name: "--search-terms",
    description: "Strings to search for when deleting entries.")
	{
	    Arity = ArgumentArity.OneOrMore
	};
```

To allow multiple arguments without repeating the option name, set [Option.AllowMultipleArgumentsPerToken](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.option.allowmultipleargumentspertoken#system-commandline-option-allowmultipleargumentspertoken) to `true`.
```C#
var searchTermsOption = new Option<string[]>(
    name: "--search-terms",
    description: "Strings to search for when deleting entries.")
	{
	    AllowMultipleArgumentsPerToken = true
	};
```
### List valid argument values

To specify a list of valid values for an option or argument, specify an enum as the option type or use [FromAmong](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.optionextensions.fromamong), as shown in the following example:
```C#
var languageOption = new Option<string>(
    "--language",
    "An option that that must be one of the values of a static list.")
        .FromAmong(
            "csharp",
            "fsharp",
            "vb",
            "pwsh",
            "sql");
```
## Model binding

A custom binder lets you combine multiple option or argument values into a complex type and pass that into a single handler parameter.

```C#
using System.CommandLine;
using System.CommandLine.Binding;

namespace SystemCommandLineWorkshop;
internal class QuoteBinder : BinderBase<Quote>
{
    private readonly Argument<string> _phraseArgument;
    private readonly Argument<string> _authorArgument;

    public QuoteBinder(Argument<string> phraseArgument, Argument<string> authorArgument)
    {
        _phraseArgument = phraseArgument;
        _authorArgument = authorArgument;
    }

    protected override Quote GetBoundValue(BindingContext bindingContext)
    {
        var phrase = bindingContext.ParseResult.GetValueForArgument(_phraseArgument) ?? throw new ArgumentNullException();
        var author = bindingContext.ParseResult.GetValueForArgument(_authorArgument) ?? throw new ArgumentNullException();
        return new Quote(phrase, author);
    }
}

...

addCommand.SetHandler((file, quote) =>
{
    AddToFile(file!, quote);
}, fileOption, new QuoteBinder(quoteArgument, bylineArgument));
```
### Custom validation and binding

To provide custom validation code, call [AddValidator](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.option.addvalidator) on your command, option, or argument.

```C#
delayOption.AddValidator(result =>
{
    if (result.GetValueForOption(delayOption) < 0)
    {
        result.ErrorMessage = "Delay value must be greather or equal 0.";
    }
});
```

If you want to parse as well as validate the input, use a `ParseArgument<T>` delegate.

```C#
var fileOption = new Option<FileInfo?>(
    name: "--file",
    description: "An option whose argument is parsed as a FileInfo",
    isDefault: true,
    parseArgument: result =>
    {
        if (result.Tokens.Count == 0)
        {
            return new FileInfo("sampleQuotes.txt");

        }
        string? filePath = result.Tokens.Single().Value;
        if (!File.Exists(filePath))
        {
            result.ErrorMessage = "File does not exist";
            return null;
        }
        else
        {
            return new FileInfo(filePath);
        }
    });
```
## Dependency injection

Use a [custom binder](https://learn.microsoft.com/en-us/dotnet/standard/commandline/model-binding#parameter-binding-more-than-16-options-and-arguments) to inject custom types into a command handler.

To configure DI, create a class that derives from [BinderBase](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.binding.binderbase-1) where `T` is the interface that you want to inject an instance for. In the [GetBoundValue](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.binding.binderbase-1.getboundvalue) method override, get and return the instance you want to inject.

Binder creation:

```C#
class LogBinder : BinderBase<ILogger>
{
    protected override ILogger GetBoundValue(BindingContext bindingContext)
    {
        return GetLogger();
    }

    ILogger GetLogger()
    {
        using ILoggerFactory loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
        ILogger logger = loggerFactory.CreateLogger("ApplicationLogger");
        return logger;
    }
}
```

Binder usage:

```C#
addCommand.SetHandler((file, quote, logger) =>
{
    AddToFile(file!, quote, logger);
}, fileOption, new QuoteBinder(quoteArgument, bylineArgument), new LogBinder());
```

Use dependency in handler:

```C#
internal static void AddToFile(FileInfo file, Quote quote, ILogger logger)
{
    logger.LogInformation($"Adding to file quote: \n{quote}");
    using StreamWriter? writer = file.AppendText();
    writer.WriteLine($"{Environment.NewLine}{Environment.NewLine}{quote.Phrase}");
    writer.WriteLine($"{Environment.NewLine}-{quote.Author}");
    writer.Flush();
    logger.LogInformation("Quote added successfully.");
}
```
## Customize help

To customize the name of an option's argument, use the option's [ArgumentHelpName](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.option.argumenthelpname#system-commandline-option-argumenthelpname) property

```C#
fileOption.ArgumentHelpName = "FILEPATH";
```

[HelpBuilder.CustomizeSymbol](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.help.helpbuilder.customizesymbol) lets you customize several parts of the help output for a command, option, or argument. With `CustomizeSymbol`, you can specify:

- The first column text.
- The second column text.
- The way a default value is described.

```C#
var parser = new CommandLineBuilder(rootCommand)
.UseDefaults()
.UseHelp(ctx =>
{
    ctx.HelpBuilder.CustomizeSymbol(fgcolorOption,
        firstColumnText: x => "--color <Black, White, Red, or Yellow>",
        secondColumnText: x => "Specifies the foreground color. " +
            "Choose a color that provides enough contrast " +
            "with the background color. " +
            "For example, a yellow foreground can't be read " +
            "against a light mode background.");
    ...
    
})
.Build();
```

You can add or replace a whole section of the help output.

```C#
var parser = new CommandLineBuilder(rootCommand)
.UseDefaults()
.UseHelp(ctx =>
{
    ...
    
    ctx.HelpBuilder.CustomizeLayout(
        _ =>
            HelpBuilder.Default
                .GetLayout()
                .Skip(1) // Skip the default command description section.
                .Prepend(
                    _ => AnsiConsole.Write(
                        new FigletText(rootCommand.Description!))
        ));
})
.Build();
```
## How to handle termination

To handle termination, inject a [CancellationToken](https://learn.microsoft.com/en-us/dotnet/api/system.threading.cancellationtoken) instance into your handler code.

```C#
readCommand.SetHandler(async (context) =>
{
    var file = context.ParseResult.GetValueForOption(fileOption);
    var delay = context.ParseResult.GetValueForOption(delayOption);
    var fgcolor = context.ParseResult.GetValueForOption(fgcolorOption);
    var lightMode = context.ParseResult.GetValueForOption(lightModeOption);

    returnCode = await ReadFile(file!, delay, fgcolor, lightMode, context.GetCancellationToken());
});
```

You can now use cancellation token.

```C#
internal static async Task<int> ReadFile(FileInfo file, int delay, ConsoleColor fgColor, bool lightMode, CancellationToken cancellationToken)
{
    Console.BackgroundColor = lightMode ? ConsoleColor.White : ConsoleColor.Black;
    Console.ForegroundColor = fgColor;
    var lines = File.ReadLines(file.FullName).ToList();
    foreach (string line in lines)
    {
        Console.WriteLine(line);
        await Task.Delay(delay * line.Length);
        if (cancellationToken.IsCancellationRequested)
        {
            return 1;
        }
    };

    return 0;
}
```
## Use Middleware

### Add to the middleware pipeline

You can add a call to this pipeline by calling [CommandLineBuilderExtensions.AddMiddleware](https://learn.microsoft.com/en-us/dotnet/api/system.commandline.builder.commandlinebuilderextensions.addmiddleware).

```C#
var parser = new CommandLineBuilder(rootCommand)
    .AddMiddleware(BenchmarkMiddleware) // Setting the middleware
    .UseDefaults()
    
	...
	
    })
    .Build();
    
private static async Task BenchmarkMiddleware(InvocationContext context, Func<InvocationContext, Task> next)
{
    if (context.ParseResult.Directives.Contains("bench"))
    {
        var sw = new Stopwatch();
        sw.Start();
        await next(context);
        sw.Stop();
        AnsiConsole.WriteLine();
        AnsiConsole.Write(new Panel($"Elapsed Time: [aqua]{sw.Elapsed}[/]"));
    }
    else
    {
        await next(context);
    }
}
```